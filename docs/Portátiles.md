# Ordenadores Portátiles

## Cromebook (Enterprise Enrollment)

1. Inicia sesión en la [consola de administraciónde Google](https://support.google.com/a/answer/182076).

2. En la consola de administración, ve a **Menú**

3. Utiliza el filtro situado en la parte superior para seleccionar el estado que tenga el dispositivo que quieras dar de baja. Para obtener más información, consulta el apartado. [Vista de estados de los dispositivos en la consola de administración](https://support.google.com/chrome/a/answer/3523633#view).

4. Si quieres aplicar la configuración a todos los usuarios, deja seleccionado el nivel organizativo superior. De lo contrario, selecciona una [unidad organizativa](https://support.google.com/a/topic/1227584) secundaria.

5. Marca la casilla situada junto al dispositivo que quieras dar de baja. **Nota**: Si quieres dar de baja varios dispositivos por motivos distintos, puedes agilizar el proceso organizándolos en grupos según el motivo de la baja.

6. En la parte superior, haz clic en **Dar de baja** dispositivos seleccionados.

7. Indica si se restablecerá el estado de fábrica de los dispositivos automáticamente. Elige la opción: **“Sí, quiero restablecer el estado de fábrica para que se eliminen todos los datos, incluidos los perfiles de usuario, las políticas del dispositivo y la información del registro**.

8. Marca la casilla para confirmar que entiendes las acciones necesarias para volver a registrar un dispositivo.

9. Selecciona el motivo de la baja.

    **Nota:** Si hay alguna licencia anual en la cuenta o si todos los dispositivos seleccionados son dispositivos Chrome Enterprise, no hace falta que selecciones ningún motivo de la baja. Para obtener más información, consulta el artículo [Acerca de los dispositivos Chrome Enterprise](https://support.google.com/chrome/a/answer/9773699).

10. Haz clic en Dar de baja.

<br>

## Cromebook  (Icloud)

1. Abra [myaccount.google.com](http://myaccount.google.com) en teléfonos inteligentes o cualquier otro sistema operativo como Windows, Linux o macOS. Lo único que necesita es el navegador. No use la misma máquina Chromebook desde donde desea eliminar / cerrar sesión en la cuenta principal.

2. Seleccione la opción Seguridad en el menú del panel lateral izquierdo y luego haga clic en **Administrar dispositivos**.

3. Ahora, podrá ver todos los dispositivos de inicio de sesión junto con el sistema operativo Google Chrome que ejecuta Chromebook.

4. Haga clic en uno de los tres puntos dados en el dispositivo Chromebook y seleccione la opción **Cerrar sesión**.

5. Vaya al ChromeBook y verá la notificación de error de inicio de sesión. Ahora, lo que tiene que hacer es simplemente cerrar la sesión del usuario principal o propietario actual de su sistema.

<br>

## Microsoft (Intune / Autopilot)

1. Inicie sesión en el [centro de administración de Microsoft Intune](https://go.microsoft.com/fwlink/?linkid=2109431).

2. En el panel **Dispositivos**, seleccione **Todos los dispositivos**.

3. Seleccione el nombre del dispositivo que quiere retirar.

4. En el panel en el que se muestra el nombre del dispositivo, haga clic en **Retirar**. Para confirmar, seleccione **Sí**.

5. Si el dispositivo está encendido y conectado, la acción **Retirar** se propaga por todos los tipos de dispositivos en menos de 15 minutos.

## Apple (Icloud)
### Si tienes aun el Mac

***En macOS Ventura o versiones posteriores:***

1. Selecciona el menú **Apple** > **Configuración del Sistema**.
    
2. Haz clic en tu nombre.

![Apple1](apple1.png)


3. El botón **Cerrar sesión** aparece debajo de la lista de los dispositivos.
    
4. Haz clic en **Cerrar sesión**.

***En macOS Monterey o versiones anteriores:***

1. Selecciona el menú **Apple** > **Preferencias del Sistema**.

2. Haz clic en **Apple ID** y, luego, en **Resumen**.

3. Haz clic en **Cerrar sesión**.
### Si ya no tienes el Mac

1. Si usas iCloud y Buscar **"mi dispositivo"** en el dispositivo, inicia sesión en [iCloud.com/find](https://icloud.com/find) o en la app **Encontrar en otro dispositivo**, 

2. Selecciona el dispositivo cuyo contenido quieras borrar y haz clic en Borrar. Una vez que el contenido del dispositivo se haya borrado, haz clic en Eliminar de la cuenta.

3. Si no puedes realizar ninguno de los pasos anteriores, cambia la contraseña de tu Apple ID. De ese modo, aunque la información personal almacenada no se elimine del dispositivo, el nuevo propietario no podrá eliminarla de iCloud.