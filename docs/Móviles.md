# Móviles

## Apple ICloud (IPAD/IPHONE)

### Si todavía tienes tu antiguo iPhone o iPad

1. Si enlazaste un Apple Watch con el iPhone, [desenlaza el Apple Watch](https://support.apple.com/es-lamr/HT204568).

2. Cierra la sesión de iCloud y de iTunes Store y App Store. 

    - Si usas iOS 10.3 o versiones posteriores, toca **Configuración** > **[tu nombre]**. Desplázate hacia abajo y toca **Cerrar sesión**. Ingresa la contraseña de tu Apple ID y toca **Desactivar**.

    - Si usas iOS 10.2 o versiones anteriores, toca **Configuración** > **iCloud** > **Cerrar sesión**. Vuelve a tocar Cerrar sesión y toca Eliminar de mi [dispositivo] e ingresa tu Apple ID y contraseña. A continuación, ve a **Configuración** > **iTunes** y **App Store** > **Apple ID** > **Cerrar sesión**.

3. Regresa a **Configuración** y toca **General** > **Transferir o restablecer [dispositivo]** > **Borrar contenido y configuración**. Si activaste **Buscar mi [dispositivo]**, es posible que debas ingresar tu Apple ID y contraseña. Si el dispositivo usa eSIM, elige la opción para borrar el contenido del dispositivo y el perfil de eSIM cuando se te solicite.

4. Si se te solicita el código del dispositivo o el código de restricciones, ingrésalo. A continuación, toca **Borrar [dispositivo]**.

5. Comunícate con tu operador para que te ayude a transferir el servicio al nuevo propietario. Si no estás usando una tarjeta SIM con el dispositivo, también puedes comunicarte con tu operador para que te ayuden a transferir el servicio al nuevo propietario.

6. [Elimina el dispositivo anterior de la lista de dispositivos de confianza](https://support.apple.com/es-lamr/HT204915#viewandmanage).

### Si ya no tienes el iPhone o iPad

Si no se realizaron los pasos anteriores o si ya no tienes el dispositivo, sigue estos pasos:

1. Si usas **iCloud** y **Buscar mi [dispositivo]** en el dispositivo, inicia sesión en [iCloud.com/find](https://icloud.com/find) o en la app **Encontrar** en otro dispositivo, selecciona el dispositivo cuyo contenido quieras borrar y haz clic en **Borrar**. Una vez que el contenido del dispositivo se haya borrado, haz clic en **Eliminar de la cuenta**.

2. Si no puedes realizar ninguno de los pasos anteriores, cambia la contraseña de tu Apple ID. De ese modo, aunque la información personal almacenada no se elimine del dispositivo, el nuevo propietario no podrá eliminarla de iCloud.

[Video explicativo (en inglés)](https://www.youtube.com/watch?v=SH7-vxJ2VAc)

## Samsung (knox)

1. Inicie sesión en [Samsung Account](https://account.samsung.com).

2. Seleccione el panel **Dispositivos**.

3. Seleccione el dispositivo que quieres eliminar.

4. Haga clic en **Desconectar**.

## Google id (Android)

1. Abra [Mis Dispositivos](https://myaccount.google.com/device-activity) en cualquier dispositivo.

2. Seleccione el dispositivo que quieres eliminar. 

3. Ahora, podrá ver todos los dispositivos que tienen tu cuenta vinculada.

4. Haga clic en uno de los tres puntos dados en el dispositivo y seleccione la opción **Cerrar sesión**.

## Microsoft (Intune / Autopilot)

1. Inicie sesión en el [centro de administración de Microsoft Intune](https://go.microsoft.com/fwlink/?linkid=2109431).

2. En el panel **Dispositivos**, seleccione **Todos los dispositivos**.

3. Seleccione el nombre del dispositivo que quiere retirar.

4. En el panel en el que se muestra el nombre del dispositivo, haga clic en **Retirar**. Para confirmar, seleccione **Sí**.

5. Si el dispositivo está encendido y conectado, la acción **Retirar** se propaga por todos los tipos de dispositivos en menos de 15 minutos.